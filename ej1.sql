); 
 

create table partidos(
    PAR_ID int unsigned not null auto_increment,
    PRO_ID int unsigned not null,
    PAR_DESCRIPCION varchar(45) not null, 
    primary key (PAR_ID),
    CONSTRAINT FK_partidos_provincia FOREIGN KEY FK_partidos_provincia (PRO_ID)
        references provincias (PRO_ID)
        on delete restrict
        on update restrict
); 

insert into provincias(PRO_DESCRIPCION) values ('Buenos aires');
insert into provincias(PRO_DESCRIPCION) values ('Córdoba');
insert into provincias(PRO_DESCRIPCION) values ('Entre Ríos');
insert into provincias(PRO_DESCRIPCION) values ('Mendoza');
insert into provincias(PRO_DESCRIPCION) values ('espacio generico');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'Ituzaingo');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'Moron');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'Ciudadela');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'Merlo');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Cruz Del Eje');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Capital');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Colón');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'General De San Martin');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Parana');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Victoria');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Gualeguay');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Gualeguaychu	');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'Junin');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'La Paz');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'Maipu');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'Rivadavia');


describe partidos;
DESCRIBE provincias; 

select * from provincias 
select * from partidos

delete from provincias 
where PRO_ID=1

delete from provincias 
where PRO_ID=5

delete from partidos
where PRO_ID=4
delete from provincias
where PRO_ID=4

